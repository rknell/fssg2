var express = require("express"),
    path = require("path"),
    compression = require('compression');

module.exports.express = {
    customMiddleware: function (app) {
        //app.use(compression());
        app.use(require('prerender-node').set('prerenderServiceUrl', process.env.PRERENDER_URL || 'http://localhost:3000/'));
        app.get('/loaderio-f71d9164bea8ea7abfec9c77f54267fc', function(req,res){
          res.send('loaderio-f71d9164bea8ea7abfec9c77f54267fc');
        })
        app.use("/", express.static(path.join(__dirname,"..", "webapp", "dist")));
        app.use("/dev", express.static(path.join(__dirname,"..", "webapp", "app")));
        app.use("/mobile", express.static(path.join(__dirname,"..", "mobileapp", "www")));

    }
};
