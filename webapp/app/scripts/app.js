'use strict';

angular
    .module('webappApp', [
        'ngRoute',
        'infinite-scroll',
        'djds4rce.angular-socialshare'
    ])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .when('/posts', {
                templateUrl: 'views/posts.html',
                controller: 'PostsCtrl'
            })
            .when("/post/:id", {
                templateUrl: "views/main.html",
                controller:"MainCtrl"
            })
            .otherwise({
                redirectTo: '/'
            });

        $locationProvider
            .html5Mode(false)
            .hashPrefix('!');
    });
