'use strict';

angular.module('webappApp')
    .controller('MasterCtrl', function ($scope, $http) {

        $http.get("/api/wallpaper")
            .success(function(data){
                var item = data[Math.floor(Math.random()*data.length)];
                $scope.backgroundStyle = "background: url('" + item.url + "'); background-size:cover;";
                console.log($scope.backgroundStyle);
            })
    });
