'use strict';

angular.module('webappApp')
    .controller('MainCtrl', function ($scope, $routeParams, posts) {
        var fapTimer = null;
        $scope.images = posts.data;
        $scope.tags = posts.tags;
        $scope.posts = posts;
        if($routeParams.id){
            console.log("Image Id", $routeParams.id);
            posts.id = $routeParams.id;
        }

        $scope.tagToggle = function (tag) {
            console.log("Toggle tag", tag);
            posts.data = [];
            $scope.images = posts.data;
            posts.tagToggle(tag);
        };

        function checkLoadNext() {
            if ((posts.data.length - $scope.slideIndex) == 5) {
                posts.loadNext();

            }
        }

        $scope.menuCss = 'menuHidden';
        $scope.toggleMenu = function () {
            console.log("clicked menu filter");
            if ($scope.menuCss) {
                $scope.menuCss = false;
            } else {
                $scope.menuCss = 'menuHidden';
            }
        };

        $scope.loadMore = function () {
            console.log("Load More Triggered...");
            posts.loadNext();
        }

        $scope.report = posts.report;


    });
