angular.module('webappApp')
    .directive('onError', function () {
        return {
            restrict: "A",
            link: function (scope, el, attr) {
                console.log("error directive loaded");
                el.bind('error', function(){
                    console.log("Image error!");
                    attr.hidden = true;
                })

            }
        }
    });