'use strict';

angular.module('webappApp')
    .factory('posts', function ($http, $timeout) {

        var loadNextTimeout = null,
            lastAfter = null,
            initial = true;

        var output = {
            data: [],
            tags: [],
            selectedTags: [],
            after: moment().toISOString(),
            canResume: false,
            id: null,
            loadNext: function () {

                if (loadNextTimeout) {
                    $timeout.cancel(loadNextTimeout);
                }

                loadNextTimeout = $timeout(function () {

                    if (lastAfter !== output.after) {
                        console.log(output.after);
                        var data = {tags: output.selectedTags};

                        if(output.id && initial){
                            data.id = output.id;
                        }

                        lastAfter = output.after;

                        $http.post("/api/post/after/" + output.after, data)
                            .success(function (data) {
                                data.forEach(function (item) {
                                    output.data.push(item);
                                    output.after = output.data[output.data.length - 1].createdAt;

                                })
                                if (!initial) {
                                    saveAfter(output.after);
                                    output.canResume = false;
                                }
                                initial = false;

                            });

                        //Log this in google analytics
                        ga('send', 'pageview');
                    }
                }, 500);
            },
            loadTags: function () {
                $http.get("/api/post/tags")
                    .success(function (data) {
                        data.forEach(function (item) {
                            output.tags.push(item);
                        });
                    })
            },

            tagToggle: function (tag) {
                console.log("Tag toggled", tag);
                var index = output.selectedTags.indexOf(tag);
                console.log(index);
                if (index != -1) {
                    output.selectedTags.splice(index, 1);
                } else {
                    output.selectedTags.push(tag);
                }
                console.log(output.selectedTags);
                output.loadNext();

            },

            resume: function () {
                console.log("Resuming session");
                while (output.data.length > 0) {
                    output.data.pop();
                }
                loadAfter();
                output.loadNext();
            },

            fromStart: function () {
                output.after = moment().toISOString();
                output.loadNext();
            },

            report: function(post){
                console.log("reporting", post);
                $http.get('/api/post/report/' + post.id)
                    .success(function(result){
                        console.log("Report result", result);
                    });

                var index = output.data.indexOf(post);
                output.data.splice(index,1);
            }
        };

        function saveAfter(date) {
            localStorage.setItem("after", date);
        }

        function loadAfter() {
            output.after = localStorage.getItem("after")
        }

        function saveTags() {
            localStorage.setItem("tags", JSON.stringify(output.tags));
        }

        function loadTags() {
            output.tags = JSON.parse(localStorage.getItem("tags"));
        }

        if (localStorage.getItem("after")) output.canResume = true;

        output.loadNext();
        output.loadTags();
        return output;
    })
;
