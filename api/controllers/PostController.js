/**
 * PostController
 *
 * @module      :: Controller
 * @description    :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

var moment = require('moment');

module.exports = {


    download: go,

    page: function (req, res) {
        page = req.params.id

//        Post.find({reported: 0})
//            .paginate({page: page, limit: 10})
//            .sort({ createdAt: 'desc' })
//            .exec(function (err, posts) {
//                if (err) res.json(err);
//                if (posts) res.json(posts);
//            });

        Post.native(function(err, post){
            post.find({reported: false})
                .sort(createdAt, 'desc')
                .skip(page*10)
                .limit(10)
                .toArray(function(result){
                    res.json(result);
                })
        })
    },

    report: function (req, res) {

        var id = req.params.id;

        Post.find({id: id})
            .exec(function (err, posts) {
                posts.forEach(function (post) {
                    if (post.reported) {
                        post.reported++;
                    } else {
                        post.reported = 1;
                    }
                    Post.update({id: post.id}, post, function(err, result){
                    })
                })
            })

    },

    after: function (req, res) {
        var date = new Date(req.params.id)

        var tags = []
        if (req.body.tags) {
            tags = (req.body.tags);
        }

        if (req.body.id) {
            //We have a post ID specified, so we need to look it up
            Post.find({id: req.body.id})
                .exec(function (err, posts) {
                    if(posts.length){
                        getNextPosts(req, res, tags, posts[0].createdAt, true)
                    } else {
                        res.json({success:false, message: "no posts found"}, 404);
                    }

                })
        } else {
            getNextPosts(req, res, tags, date)
        }


    },

    tags: function (req, res) {
        if (cache.tags.data.length) {
            res.json(cache.tags.data);
        } else {
            var mongoClient = require('mongodb').MongoClient;
            mongoClient.connect(process.env.DB_URI || "mongodb://localhost/fssg", function (err, db) {
                if (err) res.json(err);
                var collection = db.collection('post');
                collection.distinct('tags', function (err, docs) {
                    if (err) res.json(err);
                    res.json(docs);
                    cache.tags.data = docs;
                });
            })
        }
    },

    sitemap: function (req, res) {
        var header = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        var footer = '</urlset>';

        Post.find().exec(function (err, posts) {

            posts.forEach(function (post) {
                header += '<url><loc>http://www.fssguncut.com/post/' + post.id + '</loc><lastmod>' + moment(post.updatedAt).format('YYYY-MM-DD') + '</lastmod><changefreq>monthly</changefreq></url>';
            });
            header += footer;
            res.set('Content-Type', 'text/xml');
            res.send(header);
        })
    },

    /**
     * Overrides for the settings in `config/controllers.js`
     * (specific to PostController)
     */
    _config: {}


};

var cache = {
    tags: {
        data: [],
        lastUpdated: null
    }
}

var request = require("request"),
    q = require("q");


/*
 Define Subreddits and their categories to post to
 */
var SubredditList = [
    {
        subreddit: "/r/wtf",
        categories: ["Funny Shit", "WTF"]
    },
    {
        subreddit: "/r/funny",
        categories: ["Funny Shit", "Just Funny"]
    },
    {
        subreddit: "/r/adviceanimals",
        categories: ["Funny Shit", "Memes"]
    },
    {
        subreddit: "/r/fail",
        categories: ["Funny Shit", "Fails"]
    },
    {
        subreddit: "/r/demotivational",
        categories: ["Funny Shit", "Motivationals"]
    },
    {
        subreddit: "/r/nsfw_wtf",
        categories: ["Funny Shit", "WTF", "NSFW"]
    },
    {
        subreddit: "/r/leakedsnapchat",
        categories: ["Sexy Girls", "Snapchats", "Selfies"]
    },
    {
        subreddit: "/r/gonewild",
        categories: ["Sexy Girls", "Selfies"]
    },
    {
        subreddit: "/r/burstingout",
        categories: ["Sexy Girls", "Tittes"]
    },
    {
        subreddit: "/r/facebookcleavage",
        categories: ["Sexy Girls", "Titties"]
    },
    {
        subreddit: "/r/AsianHottiesGIFS",
        categories: ["Sexy Girls", "Asians", "GIFs"]
    },
    {
        subreddit: "/r/JapaneseHotties",
        categories: ["Sexy Girls", "Asians", "Japanese"]
    },
    {
        subreddit: "/r/AsianCuties",
        categories: ["Sexy Girls", "Asians"]
    },
    {
        subreddit: "/r/JuicyAsians",
        categories: ["Sexy Girls", "Asians"]
    },
    {
        subreddit: "/r/realasians",
        categories: ["Sexy Girls", "Asians"]
    },
    {
        subreddit: "/r/racequeens",
        categories: ["Sexy Girls", "Asians"]
    },
    {
        subreddit: "/r/AsiansGoneWild",
        categories: ["Sexy Girls", "Asians", "Selfies"]
    },
    {
        subreddit: "/r/AsianHotties",
        categories: ["Sexy Girls", "Asians"]
    },
    {
        subreddit: "/r/nextdoorasians",
        categories: ["Sexy Girls", "Asians"]
    },
    {
        subreddit: "/r/nextdoorasians",
        categories: ["Sexy Girls", "Asians"]
    },
    {
        subreddit: "/r/XXX_Animated_Gifs",
        categories: ["Sexy Girls", "GIFs"]
    },
    {
        subreddit: "/r/nsfw_gifs",
        categories: ["Sexy Girls", "GIFs"]
    },
    {
        subreddit: "/r/nsfw",
        categories: ["Sexy Girls"]
    },
    {
        subreddit: "/r/PhotoPlunder",
        categories: ["Sexy Girls"]
    },
    {
        subreddit: "/r/RealGirls/",
        categories: ["Sexy Girls"]
    },
    {
        subreddit: '/r/titstouchingtits',
        categories: ["Sexy Girls", "Titties"]
    }
];


/*
 Functions
 */
function downloadReddit(subreddit) {
    var deferred = q.defer();
    request("http://www.reddit.com/" + subreddit + ".json?limit=100", function (error, response, body) {
        if (!error && response.statusCode === 200) {
            var json = JSON.parse(body);
            deferred.resolve(json);
        } else {
            deferred.reject();
        }
    });
    return deferred.promise;
}

function downloadImgur(subreddit) {
    var deferred = q.defer();

    var options = {
        url: "https://api.imgur.com/3/gallery" + subreddit,
        headers: {
            Authorization: "Client-ID " + (process.env.IMGUR_CLIENT_ID || "93ffb79a0c28830")
        }
    };
    request(options, function (err, res, body) {
        try {
            var json = JSON.parse(body);
            deferred.resolve(json);
        } catch (e) {
            console.error("Error in download imgur", subreddit);
            deferred.reject(subreddit);
        }
    });

    return deferred.promise;
}
/*
 Begin the execution
 */

function go(req, res) {

    //res.json({status: "success"});

    var result = [];

    //Loop over each subreddit
    SubredditList.forEach(function (subreddit) {

        //Download the reddit JSON
        downloadImgur(subreddit.subreddit).then(function (result) {

            result.data.forEach(function (post) {


                var record = {
                    url: post.link,
                    title: post.title,
                    nsfw: post.nsfw,
                    source: subreddit.subreddit,
                    tags: []
                };

                subreddit.categories.forEach(function (category) {
                    record.tags.push(category);
                });

                if (record.nsfw) {
                    record.tags.push("NSFW")
                } else {
                    record.tags.push("SFW");
                }


                Post.create(record)
                    .done(function (err, post) {
                    });


            })


        });

        downloadReddit(subreddit.subreddit).then(function (result) {

            //res.json(result);

            var posts = result.data.children;
            res.json(posts);

            posts.forEach(function (post) {
                var data = post.data;
                var record = {
                    url: data.url,
                    title: data.title,
                    nsfw: data.over_18,
                    source: subreddit.subreddit,
                    tags: []
                };

                //Gotta copy the category otherwise the NSFW/SFW copy keeps going
                subreddit.categories.forEach(function (category) {
                    record.tags.push(category);
                });

                if (data.over_18) {
                    record.tags.push("NSFW");
                } else {
                    record.tags.push("SFW");
                }

                if (record.url.match(/\.(\w+)$/)) {
                    //It has an extension

                    var extension = record.url.match(/\.(\w+)$/);


                    if (!record.title.match(/(GORE|nsfl|death)/ig) && record.url.match) {
                        if (extension[1] !== 'html') {
                            Post.create(record)
                                .done(function (err, post) {
                                });
                        }
                    }


                }
            })

        })

    });
}

function getNextPosts(req, res, tags, date, dateEqual) {

    var operator = "<";
    if (dateEqual) {
        operator = "<=";
    }

    var dateFilter = {};
    dateFilter[operator] = date;

    var query = Post.find({reported: undefined})
        .where({createdAt: dateFilter})
        .limit(10)
        .sort({ createdAt: 'desc' })

    tags.forEach(function (tag) {
        query.where({tags: tag})
    })



    query.exec(function (err, posts) {
        if (err) res.json(err);
        if (posts) res.json(posts);
    });
}