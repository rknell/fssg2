/**
 * WallpaperController
 *
 * @module      :: Controller
 * @description    :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {

    refresh: go,


    /**
     * Overrides for the settings in `config/controllers.js`
     * (specific to WallpaperController)
     */
    _config: {}


};

function downloadImgur(subreddit) {
    var q = require('q'),
        request = require("request");

    var deferred = q.defer();

    var options = {
        url: "https://api.imgur.com/3/gallery" + subreddit,
        headers: {
            Authorization: "Client-ID " + (process.env.IMGUR_CLIENT_ID || "93ffb79a0c28830")
        }
    };
    request(options, function (err, res, body) {
        var json = JSON.parse(body);
        deferred.resolve(json);
    });

    return deferred.promise;
}

/*
 Begin the execution
 */

function go(req, res) {

    res.json({status: "success"});

    var result = [];

    var subreddit = {
        subreddit: "/r/NSFW_wallpapers"
    }


    downloadImgur(subreddit.subreddit).then(function (result) {

        result.data.forEach(function (post) {


            var record = {
                url: post.link
            }


            Wallpaper.create(record)
                .done(function (err, post) {

                });

        })


    });

}